build:
	docker-compose build  --no-cache

run:
	docker-compose up -d

down:
	docker-compose down

app_log:
	docker-compose logs -f app

create_user:
	curl --request POST \
	     --url '127.0.0.1:8080/users'\
	     --data '{"user":{"first_name": "Ayoub","last_name": "Oulaika","username": "aoulaika","email": "ayoubolk@gmail.com","country": "MA", "password":"12345678"}}'

get_user:
	curl --request GET \
	     --url '127.0.0.1:8080/users/$(id)' \
	     --data '{"fields":["username", "country","created_at", "updated_at"]}'

get_users:
	curl --request GET \
	     --url '127.0.0.1:8080/users' \
	     --data '{"filter":{"country":"MA"}}'

patch_user:
	curl --request PATCH \
	     --url '127.0.0.1:8080/users/$(id)' \
	     --data '{"user":{"last_name":"OLK"}}'

delete_user:
	curl --request DELETE \
			 --url '127.0.0.1:8080/users/$(id)'
