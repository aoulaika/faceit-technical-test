package main

import (
	"gitlab.com/aoulaika/faceit-technical-test/cmd"
	"os"

	"github.com/urfave/cli"
)

func main() {
	app := cli.NewApp()

	app.Name = "skeleton"
	app.Version = "1.0.0"
	app.Commands = []cli.Command{
		{
			Name:   "web",
			Usage:  "start the web server",
			Action: cmd.Web,
		},
	}

	_ = app.Run(os.Args)
}
