
# How to run
<details>
  <summary>expand!</summary>
This project can be deployed locally using docker compose

## App management

1. build the app `make build`
2. run the app `make run`
3. app logs `make app_log`
4. shutdown the app `make down`

## Test the app

1. create user `make create_user`
2. patch user `make patch_user id=123`
3. get user `make get_user id=123`
4. get users `make get_users`
5. delete user `make delete_user id=123`

## What can be improved

1. extract user mutex in a different package
2. mock mongoDB connection to test their function
3. better handling for http codes on conflict during user creation and update
4. check uniqueness of username & email on creation & update
5. refactor some logs


</details>

# TASK
<details>
  <summary>expand!</summary>
To write a small microservice to manage access to Users, the service should be implemented in Go - as this is primary language that we use at FACEIT for backend development.

# REQUIREMENTS

A user must be stored using the following schema:

```json
{
  "first_name": "Ayoub",
  "last_name": "Oulaika",
  "username": "aoulaika",
  "password": "******",
  "email": "ayoubolk@gmail.com",
  "country": "MA",
  "created_at": "",
  "updated_at": ""
}
```

## The service must allow you to:

Add a new User
Modify an existing User
Remove a User
Return a paginated list of Users, allowing for filtering by certain criteria (e.g. all Users with the country "MA")

## The service must:

Provide an HTTP/RPC API
Use a sensible storage mechanism for the Users
Have the ability to notify other interested services of changes to User entities
Have meaningful logs
Be well documented
Have a health check

## The service must NOT:

Provide login or authentication functionality

## Technology stack

It is up to you what technologies and patterns you use to implement these features, but you will be assessed on these choices and we expect you to be confident in explaining your choice. We encourage the use of local alternatives or stubs (for instance a database containerised and linked to your service through docker-compose).

## Notes

Remember that we want to test your understanding of these concepts, not how well you write boilerplate code. If your solution is becoming overly complex, simply explain what would have been implemented and prepare for follow-up questions in the technical interview.

Please also provide a README.md that contains:
Instructions to start the application on localhost (dockerised applications are preferred)
An explanation of the choices taken and assumptions made during development
Possible extensions or improvements to the service (focusing on scalability and deployment to production)

We expect to be able to run the tests, build the application and run it locally.

## WHAT YOU WILL BE SCORED ON

### Coding Skills:

Is your code respecting fields and access modifiers?
Is your code respecting single responsibility principles?

### Application Structure:

Have you applied the correct division of the layers?
Do you have the correct dependencies between layers?

### Framework Usage:

Have you applied the correct usage of framework features?

### REST endpoints:

Is your URL structure correct?
Have you used HTTP verbs?

### Asynchronous communication:

Is it asynchronous?

### Testing:

Are you happy with your test coverage?

</details>