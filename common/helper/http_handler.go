package helper

import (
	"encoding/json"
	"gitlab.com/aoulaika/faceit-technical-test/common/logging"
	"log"
	"net/http"
	"runtime/debug"

	"github.com/sarulabs/di/v2"
	"go.uber.org/zap"
)

// HTTPHandler is similar to http.Handler with an additional di.Container
type HTTPHandler func(w http.ResponseWriter, r *http.Request, ctn di.Container)

// CreateHTTPHandler creates a http.HandlerFunc from a HTTPHandler and a di.Container.
// It also recovers from panics in handler and log the errors.
func CreateHTTPHandler(h HTTPHandler, app di.Container) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		r.Close = true
		defer func() {
			if recovery := recover(); recovery != nil {
				var logger zap.Logger

				diErr := app.Fill(logging.DiLogger, &logger)

				_ = NewJSONResponse(map[string]interface{}{
					"name":    "internal_error",
					"message": []string{"a technical problem occurred"},
				}).Write(w, http.StatusInternalServerError)

				if diErr == nil {
					logger.
						Error(
							"panic recovered",
							zap.Any("recovery", recovery),
							zap.String("stack", string(debug.Stack())),
						)
					return
				}

				formattedErr, _ := json.Marshal(map[string]interface{}{
					"stack":    string(debug.Stack()),
					"recovery": recovery,
					"message":  "panic recovered",
				})

				log.Println(string(formattedErr))
			}
		}()

		req, _ := app.SubContainer()
		logger := req.Get(logging.DiLogger).(*zap.Logger)
		defer func() {
			if err := req.Delete(); err != nil {
				logger.Error("could not delete request container", zap.Error(err))
			}
		}()

		h(w, r, req)
	}
}
