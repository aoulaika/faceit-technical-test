package helper

import (
	"context"
	"net/http"
	"time"
)

// HTTPServerCloser ...
type HTTPServerCloser struct {
	Server  *http.Server
	Timeout time.Duration
}

// Close ...
func (c *HTTPServerCloser) Close() error {
	ctx, closer := context.WithTimeout(context.Background(), c.Timeout)
	_ = closer
	_ = c.Server.Shutdown(ctx)
	return nil
}
