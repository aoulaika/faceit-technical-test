package helper

import (
	"encoding/json"
	"github.com/pkg/errors"
	"net/http"
)

// NewJSONResponse creates a new JSONResponse.
func NewJSONResponse(content interface{}) *JSONResponse {
	return &JSONResponse{Content: content}
}

// JSONResponse is useful to write an object (map, structure)
// encoded in JSON in a http.ResponseWriter.
// e.g.: NewJSONResponse(map[string]string{"status": "ok"}).Write(w, 200)
type JSONResponse struct {
	Content interface{}
}

// Bytes returns the marshal JSONResponse content as []byte.
func (r *JSONResponse) Bytes() ([]byte, error) {
	res, err := json.Marshal(r.Content)
	return res, errors.Wrapf(err, "could not marshal data")
}

func (r *JSONResponse) Write(w http.ResponseWriter, httpCode int) error {
	w.Header().Set("Content-Type", "application/json")

	b, _ := r.Bytes()
	w.WriteHeader(httpCode)
	_, err := w.Write(b)
	return err
}
