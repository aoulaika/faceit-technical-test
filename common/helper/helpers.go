package helper

import (
	"encoding/json"
	"net/http"

	"github.com/go-playground/validator/v10"

	"go.uber.org/zap"
)

// request helper

// read request body into a struct
func decodeRequestBody(r *http.Request, req interface{}) error {
	defer r.Body.Close()
	decoder := json.NewDecoder(r.Body)
	return decoder.Decode(&req)
}

// ParseRequest read request body into a struct and respond if any error
func ParseRequest(r *http.Request, w http.ResponseWriter, logger *zap.Logger, req interface{}) error {
	if err := decodeRequestBody(r, &req); err != nil {
		logger.Error("could not parse request", zap.Error(err))
		return NewValidationErrorResponse(w, err)
	}
	return nil
}

// ValidateRequest read and request body
func ValidateRequest(r *http.Request, w http.ResponseWriter, logger *zap.Logger, req interface{}, tag string) (bool, error) {
	if err := ParseRequest(r, w, logger, &req); err != nil {
		return false, nil
	}
	v := validator.New()
	v.SetTagName(tag)
	err := v.Struct(req)
	if err != nil {
		logger.Error("could not validate request", zap.Error(err))
		return false, NewValidationErrorResponse(w, err)
	}
	return true, nil
}

// responses helper

func NewInternalErrorResponse(w http.ResponseWriter, err error) error {
	return NewJSONResponse(map[string]interface{}{
		"name":  "internal_error",
		"error": err.Error(),
	}).Write(w, http.StatusInternalServerError)
}

func NewValidationErrorResponse(w http.ResponseWriter, err error) error {
	return NewJSONResponse(map[string]interface{}{
		"name":  "invalid_request",
		"error": err.Error(),
	}).Write(w, http.StatusBadRequest)
}

func NewConflictErrorResponse(w http.ResponseWriter, err error) error {
	return NewJSONResponse(map[string]interface{}{
		"name":  "conflict",
		"error": err.Error(),
	}).Write(w, http.StatusConflict)
}

func NewOKResponse(w http.ResponseWriter, d interface{}) error {
	return NewJSONResponse(d).Write(w, http.StatusOK)
}

func NewCreationResponse(w http.ResponseWriter, d interface{}) error {
	return NewJSONResponse(d).Write(w, http.StatusCreated)
}

func NewNoContentResponse(w http.ResponseWriter) error {
	return NewJSONResponse(nil).Write(w, http.StatusNoContent)
}
