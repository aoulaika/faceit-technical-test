package common

import (
	"io"

	"github.com/sarulabs/di/v2"
	"github.com/streadway/amqp"
	"gitlab.com/aoulaika/faceit-technical-test/common/logging"
	"gitlab.com/aoulaika/faceit-technical-test/common/service"
	"go.uber.org/zap"
)

// Definitions contains all the service definitions
var Definitions = [][]di.Def{
	logging.Defs,
	service.MongoDefs,
	service.AppDefinitions,
	service.RabbitDefs,
	service.RedisDefinitions,
}

// WebBootstrap returns the di.Container
func WebBootstrap() (di.Container, io.Closer) {
	app, closer := createServices()
	createQueues(app)

	return app, closer
}

func createQueues(app di.Container) {
	logger := app.Get(logging.DiLogger).(*zap.Logger)
	ch := app.Get(service.DiRabbitChannel).(*amqp.Channel)
	logger.Debug("creating exchange user:create")
	err := ch.ExchangeDeclare(
		"user:create", // name
		"fanout",      // type
		true,          // durable
		false,         // auto-deleted
		false,         // internal
		false,         // no-wait
		nil,           // arguments
	)
	if err != nil {
		logger.Error("could not declare exchange", zap.String("exchange_name", "user:create"))
	}
	logger.Debug("creating exchange user:update")
	err = ch.ExchangeDeclare(
		"user:update", // name
		"fanout",      // type
		true,          // durable
		false,         // auto-deleted
		false,         // internal
		false,         // no-wait
		nil,           // arguments
	)
	if err != nil {
		logger.Error("could not declare exchange", zap.String("exchange_name", "user:update"))
	}
}

func createServices() (di.Container, io.Closer) {
	b, _ := di.NewBuilder()

	for _, defs := range Definitions {
		if err := b.Add(defs...); err != nil {
			panic(err)
		}
	}

	app := b.Build()
	return app, &AppCloser{
		App: app,
	}
}

type AppCloser struct {
	App di.Container
}

func (c *AppCloser) Close() error {
	return c.App.Delete()
}
