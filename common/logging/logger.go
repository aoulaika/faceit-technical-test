package logging

import (
	"encoding/json"
	"github.com/sarulabs/di/v2"
	"go.uber.org/zap"
)

const DiLogger = "logger"

var Defs = []di.Def{
	{
		Name:  DiLogger,
		Scope: di.App,
		Build: func(ctn di.Container) (interface{}, error) {
			rawJSON := []byte(`{
			   "level": "debug",
			   "encoding": "json",
			   "outputPaths": ["stdout"],
			   "errorOutputPaths": ["stderr"],
			   "encoderConfig": {
				 "messageKey": "message",
				 "levelKey": "level",
				 "levelEncoder": "lowercase"
			   }
			 }`)
			var cfg zap.Config
			if err := json.Unmarshal(rawJSON, &cfg); err != nil {
				return nil, err
			}
			return cfg.Build()

		},
		Close: func(obj interface{}) error {
			if l := obj.(*zap.Logger); l != nil {
				return l.Sync()
			}
			return nil
		},
	},
}
