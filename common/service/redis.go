package service

import (
	"context"
	"errors"
	"time"

	"github.com/bsm/redislock"
	"github.com/go-redis/redis/v8"
	"github.com/sarulabs/di/v2"
	"gitlab.com/aoulaika/faceit-technical-test/common/env"
)

const (
	DiRedis = "redis"
	DiMutex = "mutex"
)

// RedisDefinitions ...
var RedisDefinitions = []di.Def{
	{
		Name: DiRedis,
		Build: func(ctn di.Container) (interface{}, error) {
			c := redis.NewClient(&redis.Options{
				Network:  "tcp",
				Addr:     ctn.Get(DiEnv).(env.Env).RedisServer,
				Password: "",
				DB:       ctn.Get(DiEnv).(env.Env).RedisDB,
			})
			pingCtx, pingCtxCancel := context.WithTimeout(context.Background(), 2*time.Second)
			defer pingCtxCancel()
			status := c.Ping(pingCtx)
			if status.String() != "ping: PONG" {
				return nil, errors.New("could not connect to redis")
			}
			return c, nil
		},
		Close: func(obj interface{}) error {
			if r := obj.(*redis.Client); r != nil {
				return r.Close()
			}
			return nil
		},
	},
	{
		Name:  DiMutex,
		Scope: di.Request,
		Build: func(ctn di.Container) (interface{}, error) {
			return redislock.New(ctn.Get(DiRedis).(*redis.Client)), nil
		},
	},
}
