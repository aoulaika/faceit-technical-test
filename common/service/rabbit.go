package service

import (
	"github.com/sarulabs/di/v2"
	"github.com/streadway/amqp"
	"gitlab.com/aoulaika/faceit-technical-test/common/env"
)

const (
	DiRabbit        = "rabbit"
	DiRabbitChannel = "rabbit-channel"
)

var RabbitDefs = []di.Def{
	{
		Name:  DiRabbit,
		Scope: di.App,
		Build: func(ctn di.Container) (interface{}, error) {
			conn, err := amqp.Dial(ctn.Get("env").(env.Env).RabbitURI)
			if err != nil {
				return nil, err
			}
			return conn, nil
		},
		Close: func(obj interface{}) error {
			if r := obj.(*amqp.Connection); r != nil {
				return r.Close()
			}
			return nil
		},
	},
	{
		Name:  DiRabbitChannel,
		Scope: di.App,
		Build: func(ctx di.Container) (interface{}, error) {
			ch, err := ctx.Get(DiRabbit).(*amqp.Connection).Channel()
			if err != nil {
				return nil, err
			}
			return ch, nil
		},
		Close: func(obj interface{}) error {
			if ch := obj.(*amqp.Channel); ch != nil {
				return ch.Close()
			}
			return nil
		},
	},
}
