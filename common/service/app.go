package service

import (
	"github.com/bsm/redislock"
	"github.com/go-redis/redis/v8"
	"github.com/gorilla/mux"
	"github.com/sarulabs/di/v2"
	"github.com/streadway/amqp"
	"gitlab.com/aoulaika/faceit-technical-test/app/cache"
	"gitlab.com/aoulaika/faceit-technical-test/app/manager"
	"gitlab.com/aoulaika/faceit-technical-test/app/repo"
	"gitlab.com/aoulaika/faceit-technical-test/common/env"
	"gitlab.com/aoulaika/faceit-technical-test/common/logging"
	"go.mongodb.org/mongo-driver/mongo"
	"go.uber.org/zap"
)

const (
	DiEnv                = "env"
	DiRouter             = "router"
	DiUserRepoFactory    = "user-repo-factory"
	DiUserCacheFactory   = "user-cache-factory"
	DiUserManagerFactory = "user-manager-factory"
)

// AppDefinitions ...
var AppDefinitions = []di.Def{
	{
		Name: DiEnv,
		Build: func(ctn di.Container) (interface{}, error) {
			return env.GetEnv()
		},
	},
	{
		Name: DiRouter,
		Build: func(ctn di.Container) (interface{}, error) {
			return mux.NewRouter().StrictSlash(true), nil
		},
	},
	{
		Name:  DiUserRepoFactory,
		Scope: di.Request,
		Build: func(ctn di.Container) (interface{}, error) {
			return &repo.UserFactory{
				Mongo: ctn.Get(DiMongo).(*mongo.Client),
			}, nil
		},
	},
	{
		Name:  DiUserCacheFactory,
		Scope: di.Request,
		Build: func(ctn di.Container) (interface{}, error) {
			return &cache.UserFactory{
				Redis: ctn.Get(DiRedis).(*redis.Client),
			}, nil
		},
	},
	{
		Name:  DiUserManagerFactory,
		Scope: di.Request,
		Build: func(ctn di.Container) (interface{}, error) {
			return &manager.UserFactory{
				Mutex:   ctn.Get(DiMutex).(*redislock.Client),
				Repo:    ctn.Get(DiUserRepoFactory).(*repo.UserFactory),
				Cache:   ctn.Get(DiUserCacheFactory).(*cache.UserFactory),
				Channel: ctn.Get(DiRabbitChannel).(*amqp.Channel),
				Logger:  ctn.Get(logging.DiLogger).(*zap.Logger),
			}, nil
		},
	},
}
