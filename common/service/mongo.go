package service

import (
	"context"
	"time"

	"gitlab.com/aoulaika/faceit-technical-test/common/env"

	"github.com/pkg/errors"
	"github.com/sarulabs/di/v2"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"go.mongodb.org/mongo-driver/mongo/readpref"
)

const DiMongo = "mongo"

var MongoDefs = []di.Def{
	{
		Name:  DiMongo,
		Scope: di.App,
		Build: func(ctn di.Container) (interface{}, error) {
			opts := options.Client().
				SetAppName("user-app").
				SetMaxPoolSize(10).
				SetMaxConnIdleTime(time.Hour).
				ApplyURI(ctn.Get("env").(env.Env).MongoURL)

			client, err := mongo.Connect(context.TODO(), opts)
			if err != nil {
				return nil, errors.Wrap(err, "could not connect to mongo")
			}

			pingCtx, pingCtxCancel := context.WithTimeout(context.Background(), 2*time.Second)
			defer pingCtxCancel()
			err = client.Ping(pingCtx, readpref.Primary())
			return client, errors.Wrap(err, "could not ping mongo")
		},
		Close: func(obj interface{}) error {
			if m := obj.(*mongo.Client); m != nil {
				ctx, cancel := context.WithTimeout(context.Background(), 30*time.Second)
				defer cancel()
				return m.Disconnect(ctx)
			}
			return nil
		},
	},
}
