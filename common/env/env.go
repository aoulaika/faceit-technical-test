package env

import "github.com/kelseyhightower/envconfig"

// Env contains all the application environment variables.
type Env struct {
	MongoURL     string `envconfig:"mongo_url" default:"mongodb://mongo"`
	RabbitURI    string `envconfig:"rabbit_uri" default:"amqp://rabbitmq"`
	RedisServer  string `envconfig:"redis_server" default:"redis:6379"`
	RedisDB      int    `envconfig:"redis_database" default:"0"`
	ServerPort   int    `envconfig:"server_port" default:"80"`
	TimeLocation string `envconfig:"time_location" default:"Europe/Paris"`
	LogLevel     string `envconfig:"log_level" default:"debug"`
}

// GetEnv returns the Env structure filled with data from the host environment.
func GetEnv() (Env, error) {
	var env Env
	err := envconfig.Process("env", &env)
	return env, err
}
