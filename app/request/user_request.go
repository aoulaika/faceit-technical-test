package request

import (
	"gitlab.com/aoulaika/faceit-technical-test/app/entity"
	"go.mongodb.org/mongo-driver/bson"
)

type Request interface {
	GetID() string
	GetFields() []string
	Match() bson.M
	Project() bson.M
}

type Pagination struct {
	Skip, Limit int64
}
type Sort struct {
	Key   string      `json:"key"`
	Order interface{} `json:"order"  find:"eq=-1|eq=1"`
}

type Sorts []*Sort

func (ss *Sorts) D() bson.D {
	a := bson.D{}
	if ss == nil || len(*ss) == 0 {
		return a
	}
	for _, s := range *ss {
		a = append(a, bson.E{Key: s.Key, Value: s.Order})
	}
	return a
}

// Get users

type GetUsersRequest struct {
	Filter     *entity.User
	Pagination *Pagination
	Sort       Sorts `json:"sort" find:"dive"`
	Fields     []string
}

func (r *GetUsersRequest) GetID() string {
	if r == nil || r.Filter == nil {
		return ""
	}
	return r.Filter.ID
}

func (r *GetUsersRequest) GetFields() []string {
	return r.Fields
}

func (r *GetUsersRequest) Match() bson.M {
	return r.Filter.Match()
}

func (r *GetUsersRequest) Project() bson.M {
	m := bson.M{}
	for _, field := range r.Fields {
		m[field] = 1
	}
	return m
}

// Get user

type GetUserRequest struct {
	ID     string
	Fields []string
}

func (r *GetUserRequest) GetID() string {
	return r.ID
}

func (r *GetUserRequest) GetFields() []string {
	return r.Fields
}

func (r *GetUserRequest) Match() bson.M {
	if r == nil {
		return bson.M{}
	}
	return bson.M{"_id": r.ID}
}

func (r *GetUserRequest) Project() bson.M {
	if r == nil {
		return bson.M{}
	}
	m := bson.M{}
	for _, field := range r.Fields {
		m[field] = 1
	}
	return m
}

// Post user

type PostUsersRequest struct {
	User *entity.User `json:"user,omitempty" post:"required" patch:"required"`
}

// Edit user

type EditUserRequest PostUsersRequest
