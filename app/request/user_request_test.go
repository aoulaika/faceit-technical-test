package request

import (
	"gitlab.com/aoulaika/faceit-technical-test/app/entity"
	"go.mongodb.org/mongo-driver/bson"
	"reflect"
	"testing"
)

func TestGetUserRequest_GetFields(t *testing.T) {
	type test struct {
		name   string
		fields GetUserRequest
		want   []string
	}
	tests := []test{
		{
			name:   "empty",
			fields: GetUserRequest{},
			want:   nil,
		},
		{
			name:   "name field",
			fields: GetUserRequest{Fields: []string{"name"}},
			want:   []string{"name"},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := tt.fields.GetFields(); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("GetFields() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestGetUsersRequest_GetFields(t *testing.T) {
	type test struct {
		name   string
		fields GetUsersRequest
		want   []string
	}
	tests := []test{
		{
			name:   "empty",
			fields: GetUsersRequest{},
			want:   nil,
		},
		{
			name:   "name field",
			fields: GetUsersRequest{Fields: []string{"name"}},
			want:   []string{"name"},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := tt.fields.GetFields(); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("GetFields() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestGetUsersRequest_Match(t *testing.T) {
	type test struct {
		name   string
		fields GetUsersRequest
		want   bson.M
	}
	tests := []test{
		{
			name:   "empty",
			fields: GetUsersRequest{},
			want:   bson.M{},
		},
		{
			name: "country filter",
			fields: GetUsersRequest{
				Filter: &entity.User{Country: `MA`},
			},
			want: bson.M{"country": `MA`},
		},
		{
			name: "country filter",
			fields: GetUsersRequest{
				Filter: &entity.User{FirstName: "Ayoub", LastName: "Oulaika"},
			},
			want: bson.M{"first_name": "Ayoub", "last_name": "Oulaika"},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := tt.fields.Match(); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("Match() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestGetUsersRequest_Project(t *testing.T) {
	type test struct {
		name   string
		fields GetUsersRequest
		want   bson.M
	}
	tests := []test{
		{
			name:   "empty",
			fields: GetUsersRequest{},
			want:   bson.M{},
		},
		{
			name:   "name project",
			fields: GetUsersRequest{Fields: []string{"name"}},
			want:   bson.M{"name": 1},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := tt.fields.Project(); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("Project() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestGetUserRequest_GetID(t *testing.T) {
	type test struct {
		name    string
		request GetUserRequest
		want    string
	}
	tests := []test{
		{
			name:    "empty",
			request: GetUserRequest{},
			want:    "",
		},
		{
			name:    "not empty",
			request: GetUserRequest{ID: "123"},
			want:    "123",
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := tt.request.GetID(); got != tt.want {
				t.Errorf("GetID() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestGetUserRequest_Match(t *testing.T) {
	type test struct {
		name    string
		request *GetUserRequest
		want    bson.M
	}
	tests := []test{
		{
			name:    "nil request",
			request: nil,
			want:    bson.M{},
		},
		{
			name:    "valid request",
			request: &GetUserRequest{ID: "123"},
			want:    bson.M{"_id": "123"},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := tt.request.Match(); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("Match() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestGetUserRequest_Project(t *testing.T) {
	type test struct {
		name    string
		request *GetUserRequest
		want    bson.M
	}
	tests := []test{
		{
			name:    "nil request",
			request: nil,
			want:    bson.M{},
		},
		{
			name:    "valid request",
			request: &GetUserRequest{Fields: []string{"username"}},
			want:    bson.M{"username": 1},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := tt.request.Project(); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("Project() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestGetUsersRequest_GetID(t *testing.T) {
	type test struct {
		name    string
		request GetUsersRequest
		want    string
	}
	tests := []test{
		{
			name:    "empty",
			request: GetUsersRequest{},
			want:    "",
		},
		{
			name:    "not empty",
			request: GetUsersRequest{Filter: &entity.User{ID: "321"}},
			want:    "321",
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := tt.request.GetID(); got != tt.want {
				t.Errorf("GetID() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestSorts_D(t *testing.T) {
	type test struct {
		name string
		ss   Sorts
		want bson.D
	}
	tests := []test{
		{
			name: "empty sort",
			ss:   Sorts{},
			want: bson.D{},
		},
		{
			name: "sort by country",
			ss: Sorts{&Sort{
				Key:   "country",
				Order: 1,
			}},
			want: bson.D{bson.E{
				Key:   "country",
				Value: 1,
			}},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := tt.ss.D(); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("D() = %v, want %v", got, tt.want)
			}
		})
	}
}
