package manager

import (
	"context"
	"encoding/json"
	"fmt"
	"time"

	"gitlab.com/aoulaika/faceit-technical-test/app/cache"
	"gitlab.com/aoulaika/faceit-technical-test/app/entity"
	"gitlab.com/aoulaika/faceit-technical-test/app/repo"
	"gitlab.com/aoulaika/faceit-technical-test/app/request"

	"github.com/bsm/redislock"
	"github.com/streadway/amqp"
	"go.uber.org/zap"
)

type UserFactory struct {
	Mutex   *redislock.Client
	Repo    *repo.UserFactory
	Cache   *cache.UserFactory
	Channel *amqp.Channel
	Logger  *zap.Logger
}

func (f *UserFactory) Build(ctx context.Context) *User {
	return &User{
		mu:      f.Mutex,
		repo:    f.Repo.Build(ctx),
		cache:   f.Cache.Build(ctx),
		channel: f.Channel,
		ctx:     ctx,
		logger:  f.Logger,
	}
}

type User struct {
	mu      *redislock.Client
	repo    *repo.User
	cache   *cache.User
	channel *amqp.Channel
	ctx     context.Context
	logger  *zap.Logger
}

func (u *User) Get(req *request.GetUserRequest) (*entity.User, error) {
	user, err := u.cache.Get(req)
	if err != nil {
		u.logger.Warn("could not get user from cache", zap.Error(err))
	}
	if user != nil {
		return user, nil
	}
	user, err = u.repo.FindByID(req.Match(), req.Project())
	if er := u.cache.Set(user, req.Fields); er != nil {
		u.logger.Warn("could not save user in cache", zap.Error(er))
	}
	return user, err
}

func (u *User) Gets(req *request.GetUsersRequest) ([]*entity.User, error) {
	return u.repo.Find(req)
}

func (u *User) Create(user *entity.User) (interface{}, error) {
	user.SetID()
	user.SetUpdatedAt()
	user.SetCreatedAt()
	if err := user.HashPassword(); err != nil {
		return nil, fmt.Errorf("could not hash password, err: %w", err)
	}
	id, err := u.repo.Insert(user)
	if err != nil {
		b, _ := json.Marshal(map[string]interface{}{"id": id})
		if er := u.channel.Publish(
			"user:create",
			"",
			false,
			false,
			amqp.Publishing{
				ContentType: "application/json",
				Body:        b,
			},
		); er != nil {
			u.logger.Warn("could not send user creation notification")
		}
	}
	return id, err
}

func (u *User) Update(user *entity.User) error {
	user.SetUpdatedAt()
	if user.Password != "" {
		if err := user.HashPassword(); err != nil {
			return fmt.Errorf("could not hash password, err: %w", err)
		}
	}

	l, err := u.mu.Obtain(u.ctx, "update_user"+user.ID, time.Second, &redislock.Options{
		RetryStrategy: redislock.ExponentialBackoff(10*time.Millisecond, 500*time.Millisecond),
	})
	if err != nil {
		return err
	}
	defer l.Release(u.ctx)
	err = u.repo.Update(user)
	if err != nil {
		if er := u.cache.Clear(user.ID); er != nil {
			u.logger.Warn("could not clear user cache")
		}
		b, _ := json.Marshal(map[string]interface{}{"id": user.ID})
		if er := u.channel.Publish(
			"user:create",
			"",
			false,
			false,
			amqp.Publishing{
				ContentType: "application/json",
				Body:        b,
			},
		); er != nil {
			u.logger.Warn("could not send user update notification")
		}
	}
	return err
}

func (u *User) Delete(id string) error {
	l, err := u.mu.Obtain(u.ctx, "delete_user"+id, 1*time.Second, &redislock.Options{
		RetryStrategy: redislock.ExponentialBackoff(10*time.Millisecond, 500*time.Millisecond),
	})
	if err != nil {
		return err
	}
	defer l.Release(u.ctx)
	err = u.repo.Delete(id)
	if err != nil {
		if er := u.cache.Clear(id); er != nil {
			u.logger.Warn("could not clear user cache")
		}
	}
	return err
}
