package cache

import (
	"context"
	"encoding/json"
	"fmt"
	"strings"

	"github.com/go-redis/redis/v8"
	"github.com/pkg/errors"
	"gitlab.com/aoulaika/faceit-technical-test/app/entity"
	"gitlab.com/aoulaika/faceit-technical-test/app/request"
)

type UserFactory struct {
	Redis *redis.Client
}

func (uf *UserFactory) Build(ctx context.Context) *User {
	return &User{
		redis: uf.Redis,
		ctx:   ctx,
	}
}

type User struct {
	redis *redis.Client
	ctx   context.Context
}

func (u *User) Set(user *entity.User, fields []string) error {
	b, err := user.Bytes()
	if err != nil {
		return errors.Wrap(err, "could not marshal user")
	}
	u.redis.Pipeline()
	return u.redis.HSet(u.ctx, "user:"+user.ID, fieldsKey(fields), b).Err()
}

func (u *User) Get(req *request.GetUserRequest) (*entity.User, error) {
	field := fieldsKey(req.Fields)
	b, err := u.redis.HGet(u.ctx, "user:"+req.ID, field).Bytes()
	if errors.Is(err, redis.Nil) {
		return nil, nil
	}
	if err != nil {
		return nil, errors.
			Wrap(err, fmt.Sprintf("error getting keys: %s, %s from redis", "user:"+req.ID, field))
	}
	var user entity.User
	return &user, errors.Wrap(json.Unmarshal(b, &user), "error decoding redis result")
}

func (u *User) Clear(id string) error {
	return u.redis.HDel(u.ctx, "user:"+id).Err()
}

func fieldsKey(fields []string) string {
	return strings.Join(fields, "|")
}
