package cache

import (
	"context"
	"testing"

	"github.com/go-redis/redis/v8"
	"github.com/go-redis/redismock/v8"
	"gitlab.com/aoulaika/faceit-technical-test/app/entity"
	"gitlab.com/aoulaika/faceit-technical-test/app/request"
)

type beforeFunc func()

func TestUser_Get(t *testing.T) {
	db, mock := redismock.NewClientMock()
	f := User{
		redis: db,
		ctx:   context.TODO(),
	}
	type test struct {
		name    string
		before  beforeFunc
		factory *User
		key     *request.GetUserRequest
		wantErr bool
	}

	tests := []test{
		{
			name: "nil result",
			before: func() {
				mock.ExpectHGet("user:123", "").RedisNil()
			},
			factory: &f,
			key:     &request.GetUserRequest{ID: "123"},
			wantErr: false,
		},
		{
			name: "non empty result",
			before: func() {
				mock.ExpectHGet("user:123", "").SetVal("{}")
			},
			factory: &f,
			key:     &request.GetUserRequest{ID: "123"},
			wantErr: false,
		},
		{
			name: "error",
			before: func() {
				mock.ExpectHGet("user:123", "").SetErr(redis.ErrClosed)
			},
			factory: &f,
			key:     &request.GetUserRequest{ID: "123"},
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			tt.before()
			if _, err := tt.factory.Get(tt.key); (err != nil) != tt.wantErr {
				t.Errorf("User.Get() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}

func TestUser_Clear(t *testing.T) {
	db, mock := redismock.NewClientMock()
	f := User{
		redis: db,
		ctx:   context.TODO(),
	}
	tests := []struct {
		name    string
		before  beforeFunc
		factory *User
		id      string
		wantErr bool
	}{
		{
			name:    "",
			factory: &f,
			before: func() {
				mock.ExpectHDel("user:123").RedisNil()
			},
			id:      "123",
			wantErr: true,
		},
	}
	for _, tt := range tests {
		tt.before()
		t.Run(tt.name, func(t *testing.T) {
			if err := tt.factory.Clear(tt.id); (err != nil) != tt.wantErr {
				t.Errorf("User.Clear() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}

func TestUser_Set(t *testing.T) {
	db, mock := redismock.NewClientMock()
	f := User{
		redis: db,
		ctx:   context.TODO(),
	}
	type args struct {
		user   *entity.User
		fields []string
	}
	type test struct {
		name    string
		before  beforeFunc
		factory *User
		args    args
		wantErr bool
	}
	tests := []test{
		{
			name:    "nil user",
			before:  func() {},
			factory: &f,
			args: args{
				user:   nil,
				fields: nil,
			},
			wantErr: true,
		},
		{
			name: "nil user",
			before: func() {
				b, _ := (&entity.User{ID: "123"}).Bytes()
				mock.ExpectHSet("user:123", "", b).SetVal(1)
			},
			factory: &f,
			args: args{
				user:   &entity.User{ID: "123"},
				fields: nil,
			},
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			tt.before()
			if err := tt.factory.Set(tt.args.user, tt.args.fields); (err != nil) != tt.wantErr {
				t.Errorf("User.Set() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}
