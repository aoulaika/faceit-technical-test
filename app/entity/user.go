package entity

import (
	"encoding/json"
	"github.com/pkg/errors"
	"reflect"
	"strings"
	"time"

	"gitlab.com/aoulaika/faceit-technical-test/app/utils"
	"golang.org/x/crypto/bcrypt"

	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
)

type User struct {
	ID        string    `json:"id" bson:"_id" find:"isdefault"`
	FirstName string    `json:"first_name,omitempty" bson:"first_name,omitempty"`
	LastName  string    `json:"last_name,omitempty" bson:"last_name,omitempty"`
	Username  string    `json:"username,omitempty" bson:"username,omitempty" post:"required"`
	Password  string    `json:"password,omitempty" bson:"password,omitempty" post:"required,min=8" find:"isdefault"`
	Email     string    `json:"email,omitempty" bson:"email,omitempty" post:"required,email"`
	Country   string    `json:"country,omitempty" bson:"country,omitempty" post:"iso3166_1_alpha2"`
	CreatedAt time.Time `json:"created_at,omitempty" bson:"created_at,omitempty" post:"isdefault" patch:"isdefault" find:"isdefault"`
	UpdatedAt time.Time `json:"updated_at,omitempty" bson:"updated_at,omitempty" post:"isdefault" patch:"isdefault" find:"isdefault"`
}

func (u *User) Bytes() ([]byte, error) {
	if u == nil {
		return nil, errors.New("cannot marshal nil user")
	}
	return json.Marshal(u)
}

func (u *User) Match() bson.M {
	m := bson.M{}
	if u == nil {
		return m
	}
	v := reflect.ValueOf(*u)

	for i := 0; i < v.NumField(); i++ {
		bsonTag := strings.TrimSuffix(v.Type().Field(i).Tag.Get("bson"), ",omitempty")
		filterValue := v.Field(i)
		if filterValue.IsZero() {
			continue
		}
		m[bsonTag] = filterValue.String()
	}
	return m
}

func (u *User) Set() bson.M {
	m := bson.M{}
	if u == nil {
		return m
	}
	v := reflect.ValueOf(*u)

	for i := 0; i < v.NumField(); i++ {
		bsonTag := strings.TrimSuffix(v.Type().Field(i).Tag.Get("bson"), ",omitempty")
		if utils.StringInSlice(bsonTag, []string{"", "_id", "updated_at", "created_at"}) {
			continue
		}
		filterValue := v.Field(i)
		if filterValue.IsZero() {
			continue
		}
		m[bsonTag] = filterValue.String()
	}
	if !u.UpdatedAt.IsZero() {
		m["updated_at"] = u.UpdatedAt
	}
	return bson.M{"$set": m}
}

func (u *User) SetID() {
	if u == nil || u.ID != "" {
		return
	}
	u.ID = primitive.NewObjectID().Hex()
}

func (u *User) SetUpdatedAt() {
	if u == nil {
		return
	}
	u.UpdatedAt = time.Now()
}

func (u *User) SetCreatedAt() {
	if u == nil {
		return
	}
	u.CreatedAt = time.Now()
}

func (u *User) HashPassword() error {
	if u == nil {
		return nil
	}
	bytes, err := bcrypt.GenerateFromPassword([]byte(u.Password), 14)
	u.Password = string(bytes)
	return err
}

func (u *User) CheckPasswordHash(password string) bool {
	if u == nil {
		return false
	}
	return nil == bcrypt.CompareHashAndPassword([]byte(u.Password), []byte(password))
}
