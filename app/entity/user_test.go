package entity

import (
	"reflect"
	"testing"
	"time"

	"go.mongodb.org/mongo-driver/bson"
)

func TestUser_Match(t *testing.T) {
	type test struct {
		name string
		user *User
		want bson.M
	}
	tests := []test{
		{
			name: "nil user",
			user: nil,
			want: bson.M{},
		},
		{
			name: "empty user",
			user: &User{},
			want: bson.M{},
		},
		{
			name: "country filter",
			user: &User{Country: "MA"},
			want: bson.M{"country": "MA"},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := tt.user.Match(); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("Match() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestUser_SetCreatedAt(t *testing.T) {
	type test struct {
		name string
		user *User
	}
	tests := []test{
		{
			name: "nil user",
			user: nil,
		},
		{
			name: "empty user",
			user: &User{},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			tt.user.SetCreatedAt()
		})
	}
}

func TestUser_SetID(t *testing.T) {
	type test struct {
		name string
		user *User
	}
	tests := []test{
		{
			name: "nil user",
			user: nil,
		},
		{
			name: "empty user",
			user: &User{},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			tt.user.SetID()
		})
	}
}

func TestUser_SetUpdatedAt(t *testing.T) {
	type test struct {
		name string
		user *User
	}
	tests := []test{
		{
			name: "nil user",
			user: nil,
		},
		{
			name: "empty user",
			user: &User{},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			tt.user.SetUpdatedAt()
		})
	}
}

func TestUser_Set(t *testing.T) {
	type test struct {
		name string
		user *User
		want bson.M
	}
	now := time.Now()
	tests := []test{
		{
			name: "nil struct",
			user: nil,
			want: bson.M{},
		},
		{
			name: "empty struct",
			user: &User{},
			want: bson.M{"$set": bson.M{}},
		},
		{
			name: "country set",
			user: &User{Country: "MA"},
			want: bson.M{"$set": bson.M{"country": "MA"}},
		},
		{
			name: "updated_at set",
			user: &User{UpdatedAt: now},
			want: bson.M{"$set": bson.M{"updated_at": now}},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := tt.user.Set(); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("Set() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestUser_Bytes(t *testing.T) {
	type test struct {
		name    string
		user    *User
		want    string
		wantErr bool
	}
	tests := []test{
		{
			name:    "nil struct",
			user:    nil,
			want:    "",
			wantErr: true,
		},
		{
			name:    "empty struct",
			user:    &User{},
			want:    `{"id":"","created_at":"0001-01-01T00:00:00Z","updated_at":"0001-01-01T00:00:00Z"}`,
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := tt.user.Bytes()
			if (err != nil) != tt.wantErr {
				t.Errorf("User.Bytes() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(string(got), tt.want) {
				t.Errorf("User.Bytes() = %v, want %v", string(got), tt.want)
			}
		})
	}
}

func TestUser_HashPassword(t *testing.T) {
	type test struct {
		name    string
		user    *User
		wantErr bool
	}
	tests := []test{
		{
			name:    "nil struct",
			user:    nil,
			wantErr: false,
		},
		{
			name:    "nil struct",
			user:    &User{Password: "123456"},
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if err := tt.user.HashPassword(); (err != nil) != tt.wantErr {
				t.Errorf("User.HashPassword() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}

func TestUser_CheckPasswordHash(t *testing.T) {
	type test struct {
		name     string
		user     *User
		password string
		want     bool
	}
	tests := []test{
		{
			name:     "nil struct",
			user:     nil,
			password: "",
			want:     false,
		},
		{
			name:     "invalid check",
			user:     &User{Password: "123456"},
			password: "",
			want:     false,
		},
		{
			name:     "valid check",
			user:     &User{Password: "$2a$14$8jXqJQj4ltEufFFYyH3Fm.o2MiTYMIvLwxECaeiygi5wsKbusvEge"},
			password: "123456",
			want:     true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := tt.user.CheckPasswordHash(tt.password); got != tt.want {
				t.Errorf("User.CheckPasswordHash() = %v, want %v", got, tt.want)
			}
		})
	}
}
