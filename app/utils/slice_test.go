package utils

import "testing"

func TestStringInSlice(t *testing.T) {
	type args struct {
		a string
		b []string
	}
	type test struct {
		name string
		args args
		want bool
	}
	tests := []test{
		{
			name: "empty args",
			args: args{},
			want: false,
		},
		{
			name: "find string in slice",
			args: args{
				a: "a",
				b: []string{"b", "a"},
			},
			want: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := StringInSlice(tt.args.a, tt.args.b); got != tt.want {
				t.Errorf("StringInSlice() = %v, want %v", got, tt.want)
			}
		})
	}
}
