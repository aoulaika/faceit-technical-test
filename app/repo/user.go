package repo

import (
	"context"

	"gitlab.com/aoulaika/faceit-technical-test/app/entity"
	"gitlab.com/aoulaika/faceit-technical-test/app/request"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo/options"

	"go.mongodb.org/mongo-driver/mongo"
)

const (
	Database   = "faceit"
	Collection = "users"
)

// UserFactory creates repo
type UserFactory struct {
	Mongo *mongo.Client
}

// Build builds repo for the given website
func (rf *UserFactory) Build(ctx context.Context) *User {
	return &User{
		collection: rf.Mongo.Database(Database).Collection(Collection),
		ctx:        ctx,
	}
}

type User struct {
	collection *mongo.Collection
	ctx        context.Context
}

func (u *User) FindByID(filter, selector bson.M) (*entity.User, error) {
	var user entity.User
	err := u.collection.FindOne(u.ctx, filter, options.FindOne().SetProjection(selector)).Decode(&user)
	if err != nil {
		return nil, err
	}

	return &user, err
}

func (u *User) Find(req *request.GetUsersRequest) ([]*entity.User, error) {
	var users []*entity.User
	opts := options.Find().SetProjection(req.Project())
	if p := req.Pagination; p != nil {
		opts.SetSkip(p.Skip)
		opts.SetLimit(p.Limit)
	}
	if s := req.Sort; len(s) > 0 {
		opts.SetSort(s.D())
	}
	curs, err := u.collection.Find(u.ctx, req.Match(), opts)
	if err != nil {
		return nil, err
	}

	return users, curs.All(u.ctx, &users)
}

func (u *User) Insert(user *entity.User) (interface{}, error) {
	res, err := u.collection.InsertOne(u.ctx, user)
	if err != nil {
		return "", err
	}
	return res.InsertedID, nil
}

func (u *User) Update(user *entity.User) error {
	res, err := u.collection.UpdateByID(u.ctx, user.ID, user.Set())
	if err != nil {
		return err
	}
	if res.MatchedCount == 0 {
		return mongo.ErrNoDocuments
	}
	return nil
}

func (u *User) Delete(id string) error {
	res, err := u.collection.DeleteOne(u.ctx, bson.M{"_id": id})
	if err != nil {
		return err
	}
	if res.DeletedCount == 0 {
		return mongo.ErrNoDocuments
	}
	return nil
}

func (u *User) CreateIndexes() error {
	_, err := u.collection.Indexes().CreateOne(u.ctx, mongo.IndexModel{
		Keys: bson.D{bson.E{
			Key:   "username",
			Value: 1,
		}},
		Options: options.Index().SetUnique(true),
	})
	return err
}
