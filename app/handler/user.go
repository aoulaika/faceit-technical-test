package handler

import (
	"errors"
	"net/http"

	"gitlab.com/aoulaika/faceit-technical-test/app/entity"
	"gitlab.com/aoulaika/faceit-technical-test/app/manager"
	"gitlab.com/aoulaika/faceit-technical-test/app/request"
	"gitlab.com/aoulaika/faceit-technical-test/common/helper"
	"gitlab.com/aoulaika/faceit-technical-test/common/logging"
	"gitlab.com/aoulaika/faceit-technical-test/common/service"

	"github.com/bsm/redislock"
	"github.com/gorilla/mux"
	"github.com/sarulabs/di/v2"
	"go.uber.org/zap"
)

func GetUser(w http.ResponseWriter, r *http.Request, ctn di.Container) {
	logger := ctn.Get(logging.DiLogger).(*zap.Logger).With(zap.String("request", "user/get"))
	logger.Info("request received")
	var (
		err, errWritingResponse error
		req                     request.GetUserRequest
	)
	defer func(err error) {
		if err != nil {
			logger.Error("error writing response", zap.Error(err))
		}
	}(errWritingResponse)

	id, errWritingResponse := getID(w, r, logger)
	if id == "" {
		return
	}
	if helper.ParseRequest(r, w, logger, &req) != nil {
		return
	}
	req.ID = id
	user, err := ctn.
		Get(service.DiUserManagerFactory).(*manager.UserFactory).
		Build(r.Context()).
		Get(&req)

	if err != nil {
		logger.Error("could not get user", zap.Error(err))
		errWritingResponse = helper.NewInternalErrorResponse(w, err)
		return
	}
	errWritingResponse = helper.NewOKResponse(w, map[string]interface{}{"user": user})
}

func GetUsers(w http.ResponseWriter, r *http.Request, ctn di.Container) {
	logger := ctn.Get(logging.DiLogger).(*zap.Logger).With(zap.String("request", "users/get"))
	logger.Debug("request received")
	var (
		err, errWritingResponse error
		req                     request.GetUsersRequest
		users                   []*entity.User
		ok                      bool
	)

	defer func(err error) {
		if err != nil {
			logger.Error("error writing response", zap.Error(err))
		}
	}(errWritingResponse)

	if ok, errWritingResponse = helper.ValidateRequest(r, w, logger, &req, "find"); !ok {
		return
	}

	users, err = ctn.
		Get(service.DiUserManagerFactory).(*manager.UserFactory).
		Build(r.Context()).
		Gets(&req)

	if err != nil {
		logger.Error("could not get users", zap.Error(err))
		errWritingResponse = helper.NewInternalErrorResponse(w, err)
		return
	}

	errWritingResponse = helper.NewOKResponse(w, map[string]interface{}{"users": users})
}

func DeleteUser(w http.ResponseWriter, r *http.Request, ctn di.Container) {
	logger := ctn.Get(logging.DiLogger).(*zap.Logger).With(zap.String("request", "user/delete"))
	logger.Info("request received")
	var (
		err, errWritingResponse error
	)
	defer func(err error) {
		if err != nil {
			logger.Error("error writing response", zap.Error(err))
		}
	}(errWritingResponse)

	id, errWritingResponse := getID(w, r, logger)
	if id == "" {
		return
	}

	err = ctn.
		Get(service.DiUserManagerFactory).(*manager.UserFactory).
		Build(r.Context()).
		Delete(id)

	if errors.Is(err, redislock.ErrNotObtained) {
		logger.Error("conflict while deleting user", zap.Error(err))
		errWritingResponse = helper.NewConflictErrorResponse(w, err)
		return
	}
	if err != nil {
		logger.Error("could not get user", zap.Error(err))
		errWritingResponse = helper.NewInternalErrorResponse(w, err)
		return
	}
	errWritingResponse = helper.NewNoContentResponse(w)
}

func EditUser(w http.ResponseWriter, r *http.Request, ctn di.Container) {
	logger := ctn.Get(logging.DiLogger).(*zap.Logger).With(zap.String("request", "user/patch"))
	logger.Debug("request received")
	var (
		err, errWritingResponse error
		req                     request.EditUserRequest
		ok                      bool
	)

	defer func(err error) {
		if err != nil {
			logger.Error("error writing response", zap.Error(err))
		}
	}(errWritingResponse)

	id, errWritingResponse := getID(w, r, logger)
	if id == "" {
		return
	}

	if ok, errWritingResponse = helper.ValidateRequest(r, w, logger, &req, "patch"); !ok {
		return
	}
	req.User.ID = id

	err = ctn.
		Get(service.DiUserManagerFactory).(*manager.UserFactory).
		Build(r.Context()).
		Update(req.User)

	if errors.Is(err, redislock.ErrNotObtained) {
		logger.Error("conflict while updating user", zap.Error(err))
		errWritingResponse = helper.NewConflictErrorResponse(w, err)
		return
	}
	if err != nil {
		logger.Error("could not update user", zap.Error(err))
		errWritingResponse = helper.NewInternalErrorResponse(w, err)
		return
	}

	errWritingResponse = helper.NewNoContentResponse(w)
}

func PostUser(w http.ResponseWriter, r *http.Request, ctn di.Container) {
	logger := ctn.Get(logging.DiLogger).(*zap.Logger).With(zap.String("request", "user/post"))
	logger.Debug("request received")
	var (
		err, errWritingResponse error
		req                     request.PostUsersRequest
		ok                      bool
	)

	defer func(err error) {
		if err != nil {
			logger.Error("error writing response", zap.Error(err))
		}
	}(errWritingResponse)

	if ok, errWritingResponse = helper.ValidateRequest(r, w, logger, &req, "post"); !ok {
		return
	}

	id, err := ctn.
		Get(service.DiUserManagerFactory).(*manager.UserFactory).
		Build(r.Context()).
		Create(req.User)

	if err != nil {
		logger.Error("could not create user", zap.Error(err))
		errWritingResponse = helper.NewInternalErrorResponse(w, err)
		return
	}

	errWritingResponse = helper.NewCreationResponse(w, map[string]interface{}{"id": id})
}

func getID(w http.ResponseWriter, r *http.Request, logger *zap.Logger) (string, error) {
	id := mux.Vars(r)["id"]
	if id == "" {
		err := errors.New("could not get id")
		logger.Error(err.Error())
		return "", helper.NewValidationErrorResponse(w, err)
	}
	return id, nil
}
