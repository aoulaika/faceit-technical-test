package handler

import (
	"encoding/base64"
	"net/http"

	"github.com/sarulabs/di/v2"
	"gitlab.com/aoulaika/faceit-technical-test/common/logging"

	"go.uber.org/zap"
)

func Homepage(w http.ResponseWriter, r *http.Request, ctn di.Container) {
	logger := ctn.
		Get(logging.DiLogger).(*zap.Logger).
		With(zap.String("request", "/"))

	logger.Debug("")

	b64 := `QVNDSUkgQVJUIENPTUlORyBTT09OLi4u`
	b, _ := base64.StdEncoding.DecodeString(b64)
	_, _ = w.Write(b)
}
