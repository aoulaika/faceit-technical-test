FROM golang:1.17-alpine AS build

EXPOSE 80

WORKDIR /go/src/gitlab.com/aoulaika/faceit-technical-test

# install go modules

COPY ./go.mod ./go.sum ./
RUN go mod download

# test and build binary from sources
ADD . .

ARG RUN_TESTS=false
RUN if [ "$RUN_TESTS" = "true" ]; then go test -v ./...; fi && go install


FROM alpine:3.15

RUN apk --no-cache add ca-certificates tzdata

# add binary
WORKDIR /app
COPY --from=build /go/bin/faceit-technical-test .

CMD ["./faceit-technical-test", "web"]
