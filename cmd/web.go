package cmd

import (
	"fmt"
	"net/http"
	"os"
	"runtime"
	"syscall"
	"time"

	"gitlab.com/aoulaika/faceit-technical-test/app/handler"
	"gitlab.com/aoulaika/faceit-technical-test/common"
	"gitlab.com/aoulaika/faceit-technical-test/common/env"
	"gitlab.com/aoulaika/faceit-technical-test/common/helper"
	"gitlab.com/aoulaika/faceit-technical-test/common/logging"
	"gitlab.com/aoulaika/faceit-technical-test/common/service"

	"github.com/gorilla/mux"
	"github.com/urfave/cli"
	"github.com/vrecan/death"
	"go.uber.org/zap"
)

// Web ...
func Web(_ *cli.Context) {
	app, appCloser := common.WebBootstrap()
	logger := app.Get(logging.DiLogger).(*zap.Logger)
	runtime.GOMAXPROCS(runtime.NumCPU())

	r := app.Get(service.DiRouter).(*mux.Router)

	// Homepage
	r.NewRoute().Path("/").Methods(http.MethodGet).
		Handler(helper.CreateHTTPHandler(handler.Homepage, app))

	// Users
	r.NewRoute().Path("/users").Methods(http.MethodPost).
		Handler(helper.CreateHTTPHandler(handler.PostUser, app))

	r.NewRoute().Path("/users").Methods(http.MethodGet).
		Handler(helper.CreateHTTPHandler(handler.GetUsers, app))

	r.NewRoute().Path("/users/{id}").Methods(http.MethodGet).
		Handler(helper.CreateHTTPHandler(handler.GetUser, app))

	r.NewRoute().Path("/users/{id}").Methods(http.MethodDelete).
		Handler(helper.CreateHTTPHandler(handler.DeleteUser, app))

	r.NewRoute().Path("/users/{id}").Methods(http.MethodPatch).
		Handler(helper.CreateHTTPHandler(handler.EditUser, app))

	port := app.Get(service.DiEnv).(env.Env).ServerPort

	s := http.NewServeMux()

	s.Handle("/", r)

	logger.Info("listening on port", zap.Int("port", port))

	// Run server
	srv := &http.Server{Addr: fmt.Sprint(":", port), Handler: s}
	go func() {
		if err := srv.ListenAndServe(); err != nil {
			logger.Error("could not serve", zap.Error(err))
			os.Exit(1)
		}
	}()

	httpCloser := &helper.HTTPServerCloser{Server: srv, Timeout: 5 * time.Second}

	cleanExit := death.NewDeath(syscall.SIGINT, syscall.SIGTERM)
	cleanExit.SetTimeout(10 * time.Second)
	_ = cleanExit.WaitForDeath(appCloser, httpCloser)
}
